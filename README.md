# NOTES

## Run
To run this webpage please open a terminal in the root folder and run:

`npm install`

And then 

`gulp` 

## Mistery component
Unfortunately I haven't got time to do anything special for that component, so I went for a lightweight CSS solution.

## Further improvements
As you may notice the page is not production ready, I tried to give a sample of some things I normally include in the project such as Sass mixins, gulp tasks, separations of concerns (css/templates/js), etc.

In a real case scenario I would add:
- gulp scrips for minification
- move SVGs inline (via gulp plugin)
- accessibility and SEO
- at least 2 image versions (mobile / desktop)
- refactor gulp file to isolate dev and prod configurations
- handle animations globally with scss mixins
