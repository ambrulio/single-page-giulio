var gulp        = require('gulp'),
browserSync = require('browser-sync'),
sass        = require('gulp-sass'),
prefix      = require('autoprefixer'),
cp          = require('child_process'),
postcss     = require('gulp-postcss'),
csswring    = require('csswring'),
cssnano     = require('cssnano'),
del         = require('del'),
babel = require('gulp-babel'),
babelify = require('babelify'),
browserify = require('browserify'),
source      = require('vinyl-source-stream'),
sourcemaps  = require('gulp-sourcemaps'),
buffer      = require('vinyl-buffer'),
livereload  = require('gulp-livereload'),
uglify = require('gulp-uglify'),
runSequence = require('run-sequence');

//handlebars
var declare  = require('gulp-declare'),
handlebars = require('gulp-compile-handlebars'),
wrap = require('gulp-wrap'),
concat = require('gulp-concat'),
rename = require('gulp-rename');

var env = process.env.NODE_ENV || 'prod';

gulp.task('js', function(){

    return browserify({entries: 'src/scripts/main.js', debug: true})
    .transform("babelify", { presets: ["env"] })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('dist/scripts'))
    //.pipe(livereload())
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest('dist/scripts'));
});

gulp.task('styles', function () {
    var processors = [
            prefix({ browsers: ['> 5%', 'last 3 versions'] }),
            csswring,
            cssnano,
          ];
    return gulp.src('src/styles/main.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: ['scss'],
            onError: browserSync.notify
        }))
        .pipe(browserSync.reload({stream:true}))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('templates', function () {
    var templateData = {};

    options = {
        ignorePartials: true,
        partials : {
        },
        batch : ['./src/templates/'],
        helpers : {
        }
    }
    return gulp.src('src/templates/*.hbs')
        .pipe(handlebars(templateData, options))
        .pipe(rename({extname:'.html'}))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('clean', function () {
    return del('dist/**/*');
});


gulp.task('watch', function () {
    gulp.watch('src/styles/**/*.scss', ['styles']);
    gulp.watch('src/scripts/**/*.js', ['js']);
    gulp.watch('src/templates/**/*.hbs', ['templates']);
});

gulp.task('browser-sync', ['build'], function() {
    browserSync({
        server: {
            baseDir: 'dist'
        }
    });
});

gulp.task('images', function () {
    return gulp
      .src(['src/images/**'])
      .pipe(gulp.dest('dist/images'));
  });
  gulp.task('fonts', function () {
    return gulp
      .src(['src/fonts/**'])
      .pipe(gulp.dest('dist/css/fonts'));
  });

gulp.task('build', ['templates', 'styles', 'js', 'images', 'fonts']);

gulp.task('default',function(done) {
    runSequence('clean', 'browser-sync', 'watch')
});
