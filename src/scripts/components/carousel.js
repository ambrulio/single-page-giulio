
import Siema from 'siema';
import $ from 'jquery';

module.exports = (function () {
    function init() {
        const carousel = new Siema({
            selector: '.siema',
            duration: 200,
            easing: 'ease-out',
            loop:true
        }); //init carousel

        $(".prev").click(()=>{
            carousel.prev();
        })
        $(".next").click(()=>{
            carousel.next();
        })
    }

    return {
        init: init
    };
})();
