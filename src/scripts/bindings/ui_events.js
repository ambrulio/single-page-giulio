
import $ from 'jquery';

module.exports = (function() {
  function init() {
    
    $('#hamburger').click(function(){
        $(this).toggleClass('open');
        $("#menu").toggleClass('visible');
	});


  }

  return {
    init: init
  };
})();
