import uiEvents from './bindings/ui_events';
import carousel from './components/carousel';
import $ from 'jquery';

$(document).ready(function(){
    uiEvents.init();
    carousel.init();
});
